import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InternalMedcineComponent } from './sites/internal-medcine/internal-medcine.component';
import { MenuComponent } from './sites/menu/menu.component';
import { AuthComponent } from './sites/auth/auth.component';
import { AuthGuard } from './auth.guard';
import { InternalMedcineListComponent } from './sites/internal-medcine-list/internal-medcine-list.component';


const routes: Routes = [
  {path: '', redirectTo: '/login' , pathMatch:'full'},
  {path: 'internal-medcine', component: InternalMedcineComponent,
  canActivate: [AuthGuard]
  },
  {path: 'internal-medcine-list', component: InternalMedcineListComponent,
  canActivate: [AuthGuard]
  },
  {path: 'menu', component: MenuComponent, 
  canActivate: [AuthGuard]
  },
  {path: 'login', component: AuthComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  InternalMedcineComponent
];