import { Injectable, SystemJsNgModuleLoader } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators, FormArray, FormArrayName } from '@angular/forms';
import { inernalMedcin } from '../sites/models/internalMedcin.model'
import * as _ from "lodash";


@Injectable({
  providedIn: 'root'
})
export class InternalMedcinService {
  apiUrl = 'http://localhost:3000/internaMedcine'
  constructor(private _http: HttpClient) { }

  internalMedcinList: Array<any>;

  posts: Observable<any>;




formx = new FormGroup({
 
  constant: new FormControl(),
  symptomcomesgoes: new FormControl(),
  symptomgraduallyworsening: new FormControl(''),
  othersss: new FormControl(),
  scale1to10: new FormControl(),
  symptomstart: new FormControl(),
  symtomstarttime: new FormControl(),
  onMedication: new FormControl(),

  onmedications: new FormArray([]),
  doctorcare: new FormControl(),
  doctorCare: new FormArray([]),
 
            hadsurgery: new FormControl(),
            hadsurgerys: new FormArray([]),
                 
  
  smokeregularly: new FormControl(),
  amount: new FormControl(),
  duration: new FormControl(),
  yearStop: new FormControl(),
  



  drinkregularly: new FormControl('', Validators.required),
  beer: new FormControl('', Validators.required),
  nobeer: new FormControl('', Validators.required),
  japsake: new FormControl('', Validators.required),
  nojapsake : new FormControl('', Validators.required),
  wisky: new FormControl('', Validators.required),
  nowisky : new FormControl('', Validators.required),
  wine: new FormControl('', Validators.required),
  nowine : new FormControl('', Validators.required),
  other1 : new FormControl('', Validators.required),
  other : new FormControl('', Validators.required), 



  pregnant : new FormControl('', Validators.required),
  breastfeeding : new FormControl('', Validators.required),
  medicalexpenses : new FormControl('', Validators.required),
  haveinterpreter : new FormControl('', Validators.required),
  otherssss : new FormControl('', Validators.required),
   
    id_internalMedcine: new FormControl(null),
    name: new FormControl('', Validators.required),
    DateOfBirth: new FormControl('', Validators.required),
    height: new FormControl('', Validators.required),
    weight: new FormControl('', Validators.required),
    
    sex: new FormControl('', Validators.required),

    bodyPart: new FormArray([]),
    foods: new FormArray([]),
    medecines: new FormArray([]),
    prolemtoday: new FormArray([]),
    stools: new FormArray([]),
    stoolfrequency: new FormControl(''),
    symptomoccur: new FormArray([]),
    symptomlike: new FormArray([]),
   
});



  initializeFormGroup() {
    this.formx.setValue({
      id_internalMedcine: null,
      name: '',
    DateOfBirth: '',
    height: '',
    weight: '',
    food: '',
    medcine: '',
    sex: '',
    fever: '',
    cough: '',
    runnynose: '',
    phlegm: '',
    difficultybreathing: '',
    palpitation: '',
    feelsulggish: '',
    geteasilytired: '',
    shortnessofbreath: '',
    dizziness: '',
    lossapetite: '',
    vomiting: '',
    bloodystool: '',
    frequenturination: '',
    bloodyurine: '',
    weightloss: '',
    feelthirsty: '',
    hypertension: '',
    paralysis: '',
    swelling: '',
    hives: '',
    insomnia: '',
    numbness: '',
    nansea: '',
    diarrhea: '',
    itchiness: '',
    pain: '',
    advisedanotherclinic: '',
    others: '',
    grayishwhite: '',
    brown: '',
    black: '',
    bloody: '',
    watery: '',
    soft: '',
    hard: '',
    frequency: '',
    morning: '',
    daytime: '',
    evening: '',
    whileinbed: '',
    whenwakingup: '',
    irregular: '',
    otherss: '',
    constant: '',
    symptomcomesgoes: '',
    symptomgraduallyworsening: '',
    othersss: '',
    scale1to10: '',
    symptomstart: '',
    symtomstarttime: '',
    doctorCare: '',
    smokeregularly: '',
    amount: '',
    duration: '',
    yearStop: '',
    drinkregularly: '',
    nobeer: '',
    nojapsake : '',
    nowisky : '',
    nowine : '',
    other : '',
    pregnant : '',
    breastfeeding : '',
    medicalexpenses : '',
    haveinterpreter : '',
    otherssss : '',
    });
  }

  insertInternalMedcin(inernalMedcin: inernalMedcin) {
   //console.log(inernalMedcin);
    const newinternalMedcin = this._http.post(this.apiUrl, inernalMedcin)
    .subscribe(
      (res) => {
        console.log(res)
        
      }, 
      (error) => console.log(error)
    );
    return newinternalMedcin;
  }

  getInternalMedicineList(){ 
    return this._http.get<inernalMedcin[]>(this.apiUrl);
  }

}
