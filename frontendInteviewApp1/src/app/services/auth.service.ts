import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import { user } from '../sites/models/user.model';
import { Router } from '@angular/router';
import { NotificationService } from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiUrl = 'http://localhost:3000/user/login'
  constructor(private _http: HttpClient, private _router: Router, private notificationService: NotificationService) { }

  form = new FormGroup({
    username: new FormControl(),
    pwd: new FormControl()
  });

  initializeFormGroup() {
    this.form.setValue({
      username: '',
      pwd: ''
    })
  }

  login(User: user) {
    //console.log(inernalMedcin);
    const loginUser= this._http.post<{token : string}>(this.apiUrl, User)
    .subscribe(
      (res) => {
        console.log(res)
        localStorage.setItem('token', res.token);
        this._router.navigate(['/menu']);
      },
      (err) => {
        this.notificationService.warn('! Username or password not correct')
        if (err instanceof HttpErrorResponse){
          if(err.status === 401){
            this._router.navigate(['/login'])
            console.log(err)
          }
        }
      }
    );
    return loginUser;
  }

  loggedIn(){
    return !!localStorage.getItem('token')
  }

  logoutUser(){
    localStorage.removeItem('token')
    this._router.navigate(['/login'])
  }

  getToken(){
    return localStorage.getItem('token')
  }

}
