import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  constructor(private _auth: AuthService, 
              private _router: Router,
              private notificationService: NotificationService,) { }

  ngOnInit() {
  }

  login(){
    try {
      this._auth.login(this._auth.form.value);
    } catch (error) {
      console.log(error);
      this.notificationService.warn(':: username or password is not correct!'); 
    }
    
    console.log(this._auth.form.value);
    //this._router.navigate(['/menu']);
    this._auth.initializeFormGroup();
    
  }

}
