import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray, ControlContainer } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { InternalMedcinService } from 'src/app/services/internal-medcin.service';
import * as _ from "lodash";



export interface Numbers{
  value: String;
  viewValue: String;
}

@Component({
  selector: 'app-internal-medcine',
  templateUrl: './internal-medcine.component.html',
  styleUrls: ['./internal-medcine.component.css']
})
export class InternalMedcineComponent implements OnInit {
  

  numbers: Numbers[] = [
    {value: '1', viewValue:'1'},
    {value: '2', viewValue:'2'},
    {value: '3', viewValue:'3'},
    {value: '4', viewValue:'4'},
    {value: '5', viewValue:'5'},
    {value: '6', viewValue:'6'},
    {value: '7', viewValue:'7'},
    {value: '8', viewValue:'8'},
    {value: '9', viewValue:'9'},
    {value: '10', viewValue:'10'},
    {value: '11', viewValue:'11'},
    {value: '12', viewValue:'12'},
    {value: '13', viewValue:'13'},
    {value: '14', viewValue:'14'},
    {value: '15', viewValue:'15'},
    {value: '16', viewValue:'16'}
  ];
 constant=false; symptomcomesgoes=false; symptomgraduallyworsening=false;
  othersss=false; beer=false; wisky=false; japsake=false; wine=false; medicalexpenses=false; haveinterpreter=false; otherssss=false; food : boolean =true
  medcine : boolean =true;  doctorCare: string; 
  OnMedication: string;

  PersonnalInformation = "PersonnalInformation";

  doctorCarebool: Boolean;
  OnMedicationbool: Boolean;

  
  
  // Have you ever had surgery?
  public hadsurgery: string;
  public hadsurgerybool: Boolean;

  // smokeregularly
  public smokeregularly: string;
  public smokeregularlybool: Boolean;

  // drinkregularly
  public drinkregularly: string;
  public drinkregularlybool: Boolean;

  symtomstarttime: Date = new Date();

  breastfeeding= false;
  chosenYearDate: Date;
  isLinear = false;

  
  bodyParts:any = [
    {name:"head /頭", value: "head"},
    {name:"eye(right) /目(右)", value:"eyeR"},
    {name:"eye(left) /目(左)", value:"eyeL1"},
    {name: "ear(right) /耳(右)", value:"earR"},
    {name: "ear(left) /耳(左)", value:"earL"},
    {name:"nose /鼻", value:"nose"},
    {name:"mouth /口", value:"mouth"},
    {name: "throat /のど", value:"throat"},
    {name: "neck /首", value:"eaneckrL"},
    {name: "sholder /肩", value:"sholder"},
    {name:"back /背", value:"back"},
    {name: "chest /胸", value:"chest"},
    {name: "abdoment /腹", value:"abdoment"},
    {name:"groin /陰部･性器", value:"groin"},
    {name: "arm(right) /腕(右)", value:"armR"},
    {name: "arm(left) /腕(左)", value:"armL"},
    {name: "hand(right) /手(右)", value:"handR"},
    {name: "hand(left) /手(左)", value:"handL"},
    {name: "waist /腰", value:"waist"},
    {name: "buttocks /尻", value:"buttocks"},
    {name: "leg(right) /脚(右)", value:"legR"},
    {name: "leg(left) /脚(左)", value:"legL"},
    {name: "foot(right) /足(右)", value:"footR"},
    {name: "foot(left) /足(左)", value:"footL"},
    {name: "knee(right) /膝(右)", value:"kneeR"},
    {name: "knee(left) /膝(左)", value:"kneeL"}
   ];

   foods:any = [
    {name:"Fish Roe/魚卵", value:"Fish Roe"},
     {name:"Shellfish /貝類", value:"Shellfish"},
     {name:"Milk /甲殻類（エビ、カニ等）", value:"Milk"},
     {name:"Cheese /卵", value:"Cheese"},
     {name:"Buckwheat /魚卵", value:"Buckwheat"},
     {name:"Peanuts /貝類", value:"Peanuts"},
     {name:"Almonds /卵", value:"Almonds"},
     {name:"wheat /小麦", value:"wheat"},
     {name:"soy /大豆", value:"soy"},
     {name:"Kiwifruit /キウイ", value:"Kiwifruit"},
     {name:"Peaches /桃", value:"Peaches"},
     {name:"Yams /山芋", value:"Yams"},
     {name:"Avocados /アボガド", value:"Avocados"},
     {name:"Blue-skin fish (Mackerel/ Salmon/ Whitebait/ Anchovies/ Sardines)/青魚", value:""},
     {name:"Shrimp/Prawns /Crabs/Lobsters /甲殻類（エビ、カニ等 /発熱", value: "Shrimp/Prawns/Crabs/Lobsters"},
     
   ];
  
   medecines:any = [
    {name:"Alcohol /アルコール", value:"Alcohol"},
    {name:"fever reducer /解熱剤", value:"fever reducer"},
    {name:"pain killer /痛み止め", value:"pain killer"},
    {name:"antibiotics /抗生物質", value:"antibiotics"},
    {name:"medicine for stomach and bowels /胃腸薬", value:"medicine for stomach and bowels"},
    {name:"anesthetic /麻酔薬", value:"antibiotics"}
   ];
  
   problemtoday:any = [
    {name: this.translate.instant('internalMedcine.Fever') +"/発熱", value:"Fever"},
    {name: this.translate.instant('internalMedcine.Cough')+"/咳", value:"Cough"},
    {name: this.translate.instant('internalMedcine.Runnynose') +"/鼻水", value:"Runnynose"},
    {name: this.translate.instant('internalMedcine.Phlegm') +"/痰", value:"Phlegm"},
    {name: this.translate.instant('internalMedcine.Difficultybreathing')+"/息が苦しい", value:"Difficultybreathing"},
    {name: this.translate.instant('internalMedcine.Palpitation')+" /動悸", value:"Palpitation"},
    {name: this.translate.instant('internalMedcine.Feelsulggish')+" /身体がだるい", value:"Feelsulggish"},
    {name: this.translate.instant('internalMedcine.Geteasilytired')+"/疲れやすい", value:"Geteasilytired"},
    {name: this.translate.instant('internalMedcine.Shortnessof')+"/息切れ", value:"shortness of breath"},
    {name: this.translate.instant('internalMedcine.Dizziness')+" /めまい", value:"Dizziness"},
    {name: this.translate.instant('internalMedcine.Lossappetite')+" /食欲がない", value:"Lossappetite"},
    {name: this.translate.instant('internalMedcine.Vomiting')+"/嘔吐", value:"Vomiting"},
    {name: this.translate.instant('internalMedcine.Bloodystool')+"/血便", value:"Bloodystool"},
    {name: this.translate.instant('internalMedcine.Frequent')+"/頻尿", value:"frequenturination"},
    {name: this.translate.instant('internalMedcine.Bloodyurine')+"/血尿", value:"Bloodyurine"},
    {name: this.translate.instant('internalMedcine.Weightloss')+"/体重減少", value:"Weightloss"},
    {name: this.translate.instant('internalMedcine.Feelthirsty')+"/喉が渇く", value:"Feelthirsty"},
    {name: this.translate.instant('internalMedcine.Hypertension')+"/高血圧", value:"Hypertension"},
    {name: this.translate.instant('internalMedcine.Paralysis' )+"/ 麻痺", value:"Paralysis"},
    {name: this.translate.instant('internalMedcine.Swelling')+"/むくみ", value:"Swelling"},
    {name: this.translate.instant('internalMedcine.Hives')+"/じんましん", value:"Hives"},
    {name: this.translate.instant('internalMedcine.Insomnia' ), value:"Insomnia"},
    {name: this.translate.instant('internalMedcine.Numbness')+"/しびれ", value:"Numbness"},
    {name: this.translate.instant('internalMedcine.Nausea')+"/吐き気", value:"Nausea"},
    {name: this.translate.instant('internalMedcine.Diarrhea')+"/下痢", value:"Diarrhea"},
    {name: this.translate.instant('internalMedcine.Itchiness')+"/かゆみ", value:"itchiness"},
    {name: this.translate.instant('internalMedcine.Pain')+"/痛み", value:"Pain"},
    {name: this.translate.instant('internalMedcine.Other') +"/その他：", value:"others"},
    
   ]

   stools:any = [
    {name: this.translate.instant('internalMedcine.Grayishwhite') +"/ 灰白色", value:"Grayishwhite"},
    {name: this.translate.instant('internalMedcine.Brown') +"/ 茶色", value:"Brown"},
    {name: this.translate.instant('internalMedcine.Black') +"/黒色", value:"Black"},
    {name: this.translate.instant('internalMedcine.bloody') +"/血便", value:"bloody"},
    {name: this.translate.instant('internalMedcine.watery') +"/水様", value:"watery"},
    {name: this.translate.instant('internalMedcine.Soft') +"/軟便", value:"Soft"},
    {name: this.translate.instant('internalMedcine.Hard') +"/硬い便", value:"Hard"},
   ]

   symptomoccurs:any = [
    {name: this.translate.instant('internalMedcine.Morning') +"/ 朝", value:"Morning"},
    {name: this.translate.instant('internalMedcine.Daytime') +"/ 昼", value:"Daytime"},
    {name: this.translate.instant('internalMedcine.Evening') +"/ 夕方", value:"Evening"},
    {name: this.translate.instant('internalMedcine.Whileinbed') +"/ 就寝中", value:"Whileinbed"},
    {name: this.translate.instant('internalMedcine.Whenwakingup') +"/ 朝", value:"Whenwakingup"},
    {name: this.translate.instant('internalMedcine.Irregular') +"/ 朝", value:"Irregular"},
    {name: this.translate.instant('internalMedcine.Other') +"/ 朝", value:"Other"},
   ]

   symptomlikes:any = [
    {name: this.translate.instant('internalMedcine.Constant') +"/ 絶え間なく、続いている", value:"Constant"},
    {name: this.translate.instant('internalMedcine.symptomcomesgoes') +"/ 症状が出たり消えたりしている", value:"symptomcomesgoes"},
    {name: this.translate.instant('internalMedcine.symptomgraduallyworsening') +"/ 絶え間なく、続いている", value:"symptomgraduallyworsening"},
    {name: this.translate.instant('internalMedcine.Other') +"/ その他", value:"Other"},
   ]

   onmedications:any = [
    {name: this.translate.instant('internalMedcine.Coldmedicine') +"/ かぜ薬", value:"Coldmedicine"},
    {name: this.translate.instant('internalMedcine.Feverreducer') +"/ 朝解熱剤", value:"Feverreducer"},
    {name: this.translate.instant('internalMedcine.Painkiller') +"/ 痛み止め", value:"Painkiller"},
    {name: this.translate.instant('internalMedcine.Anti-suppuration') +"/ 化膿止め", value:"Anti-suppuration"},
    {name: this.translate.instant('internalMedcine.Antibiotics') +"/ 化膿止め", value:"Antibiotics"}
   ]
   
   doctorcares:any = [
    {name: this.translate.instant('internalMedcine.bronchialasthma') +"/ 気管支喘息 /発熱", value:"bronchialasthma"},
    {name: this.translate.instant('internalMedcine.highbloodpressure') +"/ 絶高血圧", value:"highbloodpressure"},
    {name: this.translate.instant('internalMedcine.Diabetesmellitus') +"/ 糖尿病", value:"Diabetesmellitus"},
    {name: this.translate.instant('internalMedcine.Tuberculosis') +"/ 結核", value:"Tuberculosis"},
    {name: this.translate.instant('internalMedcine.Hearttrouble') +"/ 心臓病", value:"Hearttrouble"},
    {name: this.translate.instant('internalMedcine.HepatitisB') +"/ Ｂ型肝炎", value:"HepatitisB"},
    {name: this.translate.instant('internalMedcine.epatitisC') +"/ Ｃ型肝炎", value:"epatitisC"},
    {name: this.translate.instant('internalMedcine.collagenConnectiveTissuedisease') +"/ 膠原病", value:"collagenConnectiveTissuedisease"},
    {name: this.translate.instant('internalMedcine.Thyroiddisease') +"/ 甲状腺の病気", value:"Thyroiddisease"},
    {name: this.translate.instant('internalMedcine.Cancertumor') +"/ 血液の病気", value:"Cancertumor"},
    {name: this.translate.instant('internalMedcine.glaucoma') +"/ 緑内障", value:"glaucoma"},
    {name: this.translate.instant('internalMedcine.Strokecerebralinfarctionhemorrhage') +"/ 脳卒中（脳梗塞・脳出血）", value:"Strokecerebralinfarctionhemorrhage"},
    {name: this.translate.instant('internalMedcine.Gastrointestinaldisorder') +"/ 胃腸の病気", value:"BlooGastrointestinaldisorderddisease"},
    {name: this.translate.instant('internalMedcine.Gonorrhea') +"/ 淋病", value:"Gonorrhea"},
    {name: this.translate.instant('internalMedcine.Syphilis') +"/ 梅毒", value:"Syphilis"}
   ]

   hadsurgerys:any = [
    {name: this.translate.instant('internalMedcine.Eye') +"/ 目", value:"Eye"},
    {name: this.translate.instant('internalMedcine.Ear') +"/ 耳", value:"Ear"},
    {name: this.translate.instant('internalMedcine.Nose') +"/ 鼻", value:"Nose"},
    {name: this.translate.instant('internalMedcine.Mouth') +"/ 口", value:"Mouth"},
    {name: this.translate.instant('internalMedcine.Throat') +"/ のど", value:"Throat"},
    {name: this.translate.instant('internalMedcine.neck') +"/ 乳房", value:"neck"},
    {name: this.translate.instant('internalMedcine.Brest') +"/ 乳房", value:"Brest"},
    {name: this.translate.instant('internalMedcine.Esophagus') +"/ 食道", value:"Esophagus"},
    {name: this.translate.instant('internalMedcine.Stomach') +"/ 胃", value:"Stomach"},
    {name: this.translate.instant('internalMedcine.Intestines') +"/ 梅毒", value:"Intestines"},
    {name: this.translate.instant('internalMedcine.Heart') +"/ 心臓<", value:"Heart"},
    {name: this.translate.instant('internalMedcine.Cecum') +"/ 盲腸", value:"Cecum"},
    {name: this.translate.instant('internalMedcine.Liver') +"/ 肝臓", value:"Liver"},
    {name: this.translate.instant('internalMedcine.Pancreas') +"/ 膵臓", value:"Pancreas"},
    {name: this.translate.instant('internalMedcine.kidney') +"/ 腎臓", value:"kidney"},
    {name: this.translate.instant('internalMedcine.Ovary') +"/ 卵巣", value:"Ovary"},
    {name: this.translate.instant('internalMedcine.Uterus') +"/ 子宮", value:"Uterus"},
    {name: this.translate.instant('internalMedcine.Upperlowerlimb') +"/ 上下肢", value:"EUpperlowerlimbye"},
    {name: this.translate.instant('internalMedcine.Others') +"/ その他", value:"Others"}
   ]

  constructor(private formBuider: FormBuilder, private translate: TranslateService, private service: InternalMedcinService) { 
    translate.setDefaultLang('en');
    translate.use('en');
    
  }

  

  ngOnInit() { 
    this.translate.setDefaultLang('en'); 
    this.translate.use('en');
  }

  

onCheckboxChange(e) {
  const checkArray: FormArray = this.service.formx.get('bodyPart') as FormArray;

  if(e.checked) {
    checkArray.push(new FormControl(e.source.value))
  } else {
    const i = checkArray.controls.findIndex(x => x.value === e.source.value);
    checkArray.removeAt(i);
  }
}

onCheckboxChangefoods(e) {
  const foodArray: FormArray = this.service.formx.get('foods') as FormArray;

  if(e.checked) {
    foodArray.push(new FormControl(e.source.value))
  } else {
    const i = foodArray.controls.findIndex(x => x.value === e.source.value);
    foodArray.removeAt(i);
  }
}

onCheckboxChangemedecines(e) {
  const medecinesArray: FormArray = this.service.formx.get('medecines') as FormArray;

  if(e.checked) {
    medecinesArray.push(new FormControl(e.source.value))
  } else {
    const i = medecinesArray.controls.findIndex(x => x.value === e.source.value);
    medecinesArray.removeAt(i);
  }
}

onCheckboxChangeProblemToday(e) {
  const problemTodayArray: FormArray = this.service.formx.get('prolemtoday') as FormArray;

  if(e.checked) {
    problemTodayArray.push(new FormControl(e.source.value))
  } else {
    const i = problemTodayArray.controls.findIndex(x => x.value === e.source.value);
    problemTodayArray.removeAt(i);
  }
}
 
onCheckboxChangestool(e) {
  const stoolArray: FormArray = this.service.formx.get('stools') as FormArray;

  if(e.checked) {
    stoolArray.push(new FormControl(e.source.value))
  } else {
    const i = stoolArray.controls.findIndex(x => x.value === e.source.value);
    stoolArray.removeAt(i);
  }
}

onCheckboxChangeSymptomoccur(e) {
  const SymptomArray: FormArray = this.service.formx.get('symptomoccur') as FormArray;

  if(e.checked) {
    SymptomArray.push(new FormControl(e.source.value))
  } else {
    const i = SymptomArray.controls.findIndex(x => x.value === e.source.value);
    SymptomArray.removeAt(i);
  }
}

onCheckboxChangesymptomlike(e) {
  const symptomlikeArray: FormArray = this.service.formx.get('symptomlike') as FormArray;

  if(e.checked) {
    symptomlikeArray.push(new FormControl(e.source.value))
  } else {
    const i = symptomlikeArray.controls.findIndex(x => x.value === e.source.value);
    symptomlikeArray.removeAt(i);
  }
}

onCheckboxChangeonmedications(e) {
  const ommedicationsArray: FormArray = this.service.formx.get('onmedications') as FormArray;

  if(e.checked) {
    ommedicationsArray.push(new FormControl(e.source.value))
  } else {
    const i = ommedicationsArray.controls.findIndex(x => x.value === e.source.value);
    ommedicationsArray.removeAt(i);
  }
}

onCheckboxChangeunderdoctorcare(e) {
  const underdoctorcareArray: FormArray = this.service.formx.get('doctorCare') as FormArray;

  if(e.checked) {
    underdoctorcareArray.push(new FormControl(e.source.value))
  } else {
    const i = underdoctorcareArray.controls.findIndex(x => x.value === e.source.value);
    underdoctorcareArray.removeAt(i);
  }
}

onCheckboxChangeunderhadsurgerys(e) {
  const hadsurgerysArray: FormArray = this.service.formx.get('hadsurgerys') as FormArray;

  if(e.checked) {
    hadsurgerysArray.push(new FormControl(e.source.value))
  } else {
    const i = hadsurgerysArray.controls.findIndex(x => x.value === e.source.value);
    hadsurgerysArray.removeAt(i);
  }
}


  toggleFood(){
    this.food = !this.food;
  }

  toggleMedcine(){
    this.medcine = !this.medcine;
  }

  toggleOnMedcation(){
    this.OnMedicationbool = JSON.parse(this.OnMedication);
  }

  toggleDoctorCare(){
   this.doctorCarebool = JSON.parse(this.doctorCare);
  }

  togglehadsurgery(){
    this.hadsurgerybool = JSON.parse(this.hadsurgery);
     console.log(this.hadsurgery);
  }

  togglehadsmokeregularly(){
    //console.log(this.smokeregularly);
    this.smokeregularlybool = JSON.parse(this.smokeregularly);
    
  }

  toggledrinkregularly(){
    //console.log(this.smokeregularly);
    this.drinkregularlybool = JSON.parse(this.drinkregularly);
    
  }

  useLanguage(Lang: string) {
    this.translate.use(Lang);
  }

  onSubmit(){

    console.log(this.service.formx.value);
    this.service.insertInternalMedcin(this.service.formx.value);
      //this.service.formx.reset();
      //this.service.initializeFormGroup();

   /*if(this.service.formx.valid){
      this.service.insertInternalMedcin(this.service.formx.value);
      this.service.formx.reset();
      this.service.initializeFormGroup();
   
    }*/
  }



  
}
