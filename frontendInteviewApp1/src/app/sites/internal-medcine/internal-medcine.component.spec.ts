import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternalMedcineComponent } from './internal-medcine.component';

describe('InternalMedcineComponent', () => {
  let component: InternalMedcineComponent;
  let fixture: ComponentFixture<InternalMedcineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternalMedcineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternalMedcineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
