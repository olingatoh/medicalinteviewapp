import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternalMedcineListComponent } from './internal-medcine-list.component';

describe('InternalMedcineListComponent', () => {
  let component: InternalMedcineListComponent;
  let fixture: ComponentFixture<InternalMedcineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternalMedcineListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternalMedcineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
