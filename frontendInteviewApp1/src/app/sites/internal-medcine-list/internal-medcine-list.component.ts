import { Component, OnInit, ViewChild } from '@angular/core';
import { inernalMedcin} from '../models/internalMedcin.model';
import { from } from 'rxjs';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { InternalMedcinService } from 'src/app/services/internal-medcin.service';

@Component({
  selector: 'app-internal-medcine-list',
  templateUrl: './internal-medcine-list.component.html',
  styleUrls: ['./internal-medcine-list.component.css']
})
export class InternalMedcineListComponent implements OnInit {

  categori$: inernalMedcin[];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort, {static: false}) sort : MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  searchKey: String;

  displayedColumns: string[] = [ 'Patient Name', 'sex', 'Height', 'Weight', 'Date of birth', 'actions'];
  dateToday = new Date();
  
  constructor(private internalMedcineService: InternalMedcinService) { }

  ngOnInit() {
    this.internalMedcineService.getInternalMedicineList().subscribe(
      list => {
        let array =list.map(item => {
          return{
            id_internalMedcine: item.id_internalMedcine,
            ...item
          }
        });
        this.dataSource = new MatTableDataSource(array);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    )};

}
