import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {TranslateModule, TranslateLoader, TranslateService} from '@ngx-translate/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InternalMedcineComponent } from './sites/internal-medcine/internal-medcine.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSidenavModule, MatNativeDateModule } from '@angular/material';
import { MaterialModule } from './material.module';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MenuComponent } from './sites/menu/menu.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { I18nModule } from './i18n/i18n.module';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { InternalMedcinService } from './services/internal-medcin.service';
import { AuthComponent } from './sites/auth/auth.component';
import { AuserComponent } from './sites/auser/auser.component';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './auth.guard';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { NotificationService } from './services/notification.service';
import { InternalMedcineListComponent } from './sites/internal-medcine-list/internal-medcine-list.component';


@NgModule({
  declarations: [
    AppComponent,
    InternalMedcineComponent,
    MenuComponent,
    AuthComponent,
    AuserComponent,
    InternalMedcineListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    NgxMaterialTimepickerModule,
    I18nModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
          useFactory: createTranslateLoader, // exported factory function needed for AoT compilation
          deps: [HttpClient]
      }
    })
  ],
  providers: [AuthService, AuthGuard, InternalMedcinService, NotificationService, TranslateService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  }
],
  bootstrap: [AppComponent],
  entryComponents:[InternalMedcineComponent]
})
export class AppModule { }
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http);
}